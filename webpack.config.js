const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')

module.exports = (env) => {
  const base = {
    entry: './src/index.js',
    output: {
      path: path.join(__dirname, 'build'),
      filename: 'bundle-[contenthash].js',
    },
    module: {
      rules: [
        {
          loader: 'babel-loader',
          test: /\.js$/,
          exclude: /node_modules/,
        },
        {
          test: /\.(png|jpe?g|gif)$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                publicPath: 'images',
                outputPath: 'images',
                name: '[name]-[contenthash].[ext]',
              },
            },
          ],
        },
        {
          test: /\.(woff|woff2|ttf|eot|otf)$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                publicPath: 'fonts',
                outputPath: 'fonts',
                name: '[name]-[contenthash].[ext]',
              },
            },
          ],
        },
      ],
    },
    plugins: [
      new webpack.ProgressPlugin(),
      new HtmlWebpackPlugin({
        title: 'React Webpack Stater Project',
        template: './public/index.html',
        filename: 'index.html',
      }),
    ],
  }

  const productionConfig = {
    ...base,
    module: {
      rules: [
        ...base.module.rules,
        {
          test: /\.s?css$/,
          use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
        },
      ],
    },
    plugins: [
      ...base.plugins,
      new MiniCssExtractPlugin({
        filename: 'style-[contenthash].css',
      }),
      new CleanWebpackPlugin(),
    ],
    devtool: 'source-map',
  }

  const developConfig = {
    ...base,
    module: {
      rules: [
        ...base.module.rules,
        {
          test: /\.s?css$/,
          use: ['style-loader', 'css-loader', 'sass-loader'],
        },
      ],
    },
    devtool: 'inline-source-map',
    devServer: {
      contentBase: path.join(__dirname, 'build'),
      port: 3000,
    },
  }

  return env === 'production' ? productionConfig : developConfig
}

import React from 'react'
import logo from './logo.png'
import './app.scss'

export default function App() {
  const repo = 'https://gitlab.com/mrprogrammer'

  return (
    <div className="container">
      <img src={logo} alt="logo" className="logo" />
      <h1>Hello React</h1>
      <h3>React Webpack Stater Project</h3>
      <a href={repo} alt="gitlab repo">
        {repo}
      </a>
    </div>
  )
}
